## financial_freedom
# 财务自由

***
    分阶段实现财务自由：  
    一阶段：财务自知  
    列出每个月详细的支出。  
    衣：  
    食：  
    住：  
    行：  
    杂项：  
    得出现有生活标准的月支出额度：月支出  
***
    二阶段：财务生存  
    应急钱储备，不到万不得已不动用的钱。  
    储备方式：  
    1.月支出*1 存储在余额宝、浦发宝之类的货币基金当中，作为当月的生活用费。  
    2.月支出*6 存储在理财产品当中，作为应急储备。  
***
    三阶段：财务保障  
    月支出*18 存储在理财产品当中。  
***
    四阶段：财务自足  
    储备足够的资金，使得理财的盈利等于月支出*1。  
    投资分配比例：  
    1.理财60%  
    2.风险投资40%  
***
    五阶段：财务舒适  
    进行更高盈利的投资项目，使得盈利水平达到月支出*2。  
    投资分配比例：  
    1.理财60%  
    2.风险投资40%  
***
    六阶段：财务解锁  
    进行更高盈利的投资项目，使得盈利水平达到月支出*5。  
    投资分配比例：  
    1.理财0%  
    2.风险投资100%  
***
    七阶段：财务自由  
    进行更高盈利的投资项目，使得盈利水平达到月支出*10。  
    投资分配比例：  
    1.理财0%  
    2.风险投资100%  
***
    本方案使用的资金池有如下几类：  
    货币基金：月支出*1  
    理财产品：月支出*6~18 + 月支出*12/利率 * 1~2  
    风险投资：月支出*12/利率 * 1~7  
    更高级资金池的额度取决于上一级资金池已经达到要求。  
***
    财务理想上限：  
    即赚够了从当前年龄到65岁退休前所需要花费的全部支出。  
***
