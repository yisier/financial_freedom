# coding: UTF-8
# 财务计算

import math
import datetime
import matplotlib.pyplot as plt


class financial:

    def __init__(self, age):
        # 年龄
        self.age = age
        # 当前年份
        self.year = datetime.datetime.now().year
        # 资产池
        self.pool = [{
            'name': 'total_funds',
            'num': 0,
            'min': 0,
            'max': 5000000000,
        }]
        # 收入池
        self.income = []
        # 支出池
        self.outlay = []

    # 添加资金池
    def add_pool(self, name='default', num=0, priority=0, min=0, max=0, rate=0.04):
        for getpool in self.pool:
            if getpool['name'] == name:
                print('add_pool error!This pool already exists.\n')
                return
        addpool = {
            'name': name,
            'num': num,
            'priority': priority,
            'min': min,
            'max': max,
            'rate': rate,
        }
        self.pool[0]['num'] = self.pool[0]['num']+addpool['num']
        self.pool.append(addpool)

    # 添加收入
    def add_income(self, name='default', num=0, mode='m', rate=0.07):
        for getincome in self.income:
            if getincome['name'] == name:
                print('add_income error!This income already exists.\n')
                return
        addincome = {
            'name': name,
            'num': num,
            'mode': mode,
            'rate': rate,
        }
        self.income.append(addincome)

    # 添加支出
    def add_outlay(self, name='default', num=0, mode='m', rate=0.07):
        for getoutlay in self.outlay:
            if getoutlay['name'] == name:
                print('add_outlay error!This outlay already exists.\n')
                return
        addoutlay = {
            'name': name,
            'num': num,
            'mode': mode,
            'rate': rate,
        }
        self.outlay.append(addoutlay)

    # 获取某一收入
    def get_income(self, name='default', year=0):
        for getincome in self.income:
            if getincome['name'] == name:
                return getincome['num']*pow(getincome['rate']+1, year)

    # 获取某一支出
    def get_outlay(self, name='default', year=0):
        for getoutlay in self.outlay:
            if getoutlay['name'] == name:
                return getoutlay['num']*pow(getoutlay['rate']+1, year)

    # 获取某一资金池的资产
    def get_funds(self, name='total_funds', year=0):
        calpool = self.pool
        for i in range(year):
            #
            pass
        for getpool in calpool:
            if getpool['name'] == name:
                return getpool['num']

    # 获取总资产
    def get_total_funds(self, year=0):
        calpool = self.pool
        for i in range(year):
            # 获取当年所有的收入
            # 获取当年所有的支出
            # 更改当年的资产池
            # 资产池产生利息
            pass
        return calpool[0]['num']


my = financial(age=26)

my.add_pool(name='fund', num=50000, priority=1,
            min=0, max=200000, rate=0.051)
my.add_pool(name='bank', num=0, priority=2,
            min=200000, max=4000000, rate=0.0426)
my.add_pool(name='stock', num=0, priority=3,
            min=0, max=20000000, rate=0.11)
print(my.pool)

my.add_income(name='pay', num=7000, mode='m', rate=0.07)
my.add_outlay(name='life', num=3000, mode='m', rate=0.04)

print('get_income pay  %d' % my.get_income(name='pay', year=10))
print('get_outlay life %d' % my.get_outlay(name='life', year=10))

print('get_funds fund  %d' % my.get_funds(name='fund', year=10))
print('get_funds bank  %d' % my.get_funds(name='bank', year=10))
print('get_funds stock %d' % my.get_funds(name='stock', year=10))
print('total_funds     %d' % my.get_total_funds(year=10))
print(my.pool)
