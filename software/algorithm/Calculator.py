# coding: UTF-8
# 财务自由计算器
import datetime
import matplotlib.pyplot as plt

"""
分阶段实现财务自由：
一阶段：财务自知
列出每个月详细的支出。
衣：
食：
住：
行：
杂项：
得出现有生活标准的月支出额度：月支出

二阶段：财务生存
应急钱储备，不到万不得已不动用的钱。
储备方式：
月支出*12 存储在货币基金、理财产品当中，作为生活用费。

三阶段：财务自足
应急储备超过月支出*12的部分购买大额存单，月月利息收入达到月支出*1
风险投资比例10%

四阶段：财务舒适
应急储备超过月支出*12的部分购买大额存单，月月利息收入达到月支出*2
风险投资比例100%

五阶段：财务解锁
大额存单利息收入超过月支出*2的部分进行风险投资。总收益达到月支出*5
风险投资比例100%

六阶段：财务自由
大额存单利息收入超过月支出*2的部分进行风险投资。总收益达到月支出*10
风险投资比例100%

本方案使用的资金池有如下几类：
货币基金：月支出*12
大额存单：月支出*12/利率 * 1~2
风险投资：剩余部分
更高级资金池的额度取决于上一级资金池已经达到要求。

财务理想上限：
即赚够了从当前年龄到65岁退休前所需要花费的全部支出。
"""

# 月支出
Monthly_expenditure = 0.6
# 月工资
Monthly_salary = 1.0
# GDP，工资水平增长率
Monthly_salary_rate = 0.08
# 消费物价增长率
Devaluation_rate = 0.03
# 货币基金利率
Monetary_Fund_rate = 0.03
# 大额存单
Financial_management_rate = 0.0426
# 风险投资利率
Venture_investment_rate = 0.15
# 资金池
Monetary_Fund = [0]
Financial_management = [0]
Venture_investment = [0]
Total_funds = [Monetary_Fund[0] +
               Financial_management[0] + Venture_investment[0]]
# 年利润
Annual_profit = [0]

# 财务阶段
Financial_stage = 0
# 当前年份
year = datetime.datetime.now().year
# 年龄
age = year - 1992
# 财务自足资金量
Financial_comfort = Monthly_expenditure * 12 / Financial_management_rate
# 财务理想上限
Financial_cap = Monthly_expenditure * 12 * (65 - age)
print("%d岁开始，月支出%.2f，月收入%.2f，总资产%d，财务自足资金量：%d，财务理想上限%d"
      % (age, Monthly_expenditure, Monthly_salary, Total_funds[0], Financial_comfort, Financial_cap))
# 各阶段坐标
xage = [65, 65, 65, 65, 65]
yfunds = [0, 0, 0, 0, 0]

# 计算至65岁的资金更迭
for i in range(1, (66 - age)):
    Monetary_Fund.append((Monthly_salary - Monthly_expenditure)
                         * 12 + Monetary_Fund[i - 1])  # 月收入储蓄
    Financial_management.append(Financial_management[i - 1])
    Venture_investment.append(Venture_investment[i - 1])
    Total_funds.append(Total_funds[i - 1])
    Annual_profit.append(0)
    if Monetary_Fund[i] > Monthly_expenditure * 12:  # 如果货币基金资金量大于月支出*12
        Financial_management[i] += Monetary_Fund[i] - Monthly_expenditure * 12
        Monetary_Fund[i] = Monthly_expenditure * 12
    if Financial_management[i] > Financial_comfort * 1:  # 如果达到了财务自足
        Venture_investment[i] += (Financial_management[i] -
                                  Financial_management[i - 1]) * 0.1
        Financial_management[i] -= Venture_investment[i] - \
            Venture_investment[i - 1]
    if Financial_management[i] > Financial_comfort * 2:  # 如果达到了财务舒适
        Venture_investment[i] += Financial_management[i] - \
            Financial_comfort * 2
        Financial_management[i] = Financial_comfort * 2

    # 计算工资增幅
    Monthly_salary *= (1 + Monthly_salary_rate)
    # 计算物价增幅和新的月支出
    Monthly_expenditure *= (1 + Devaluation_rate)
    Financial_comfort = Monthly_expenditure * 12 / Financial_management_rate

    # 利息计算
    Monetary_Fund[i] *= (1 + Monetary_Fund_rate)
    Financial_management[i] *= (1 + Financial_management_rate)
    Venture_investment[i] *= (1 + Venture_investment_rate)
    # 总资产计算
    Total_funds[i] = Monetary_Fund[i] + \
        Financial_management[i] + Venture_investment[i]
    # 年利润计算，除去工资
    Annual_profit[i] = Total_funds[i] - Total_funds[i - 1] - Monthly_salary*12

    # 输出财务阶段
    if Monetary_Fund[i] > Monthly_expenditure * 12 and Financial_stage == 0:
        xage[Financial_stage] = age + i
        yfunds[Financial_stage] = Total_funds[i]
        Financial_stage = 1
        print("%d岁 %d财务生存：月支出%.2f，月收入%.2f，大额存单%d，风险投资%d，年利润%.2f"
              % (age + i, year + i, Monthly_expenditure, Monthly_salary, Financial_management[i], Venture_investment[i], Annual_profit[i]))
    elif Financial_management[i] > Financial_comfort and Financial_stage == 1:
        xage[Financial_stage] = age + i
        yfunds[Financial_stage] = Total_funds[i]
        Financial_stage = 2
        print("%d岁 %d财务自足：月支出%.2f，月收入%.2f，大额存单%d，风险投资%d，年利润%.2f"
              % (age + i, year + i, Monthly_expenditure, Monthly_salary, Financial_management[i], Venture_investment[i], Annual_profit[i]))
    elif Financial_management[i] > Financial_comfort * 2 and Financial_stage == 2:
        xage[Financial_stage] = age + i
        yfunds[Financial_stage] = Total_funds[i]
        Financial_stage = 3
        print("%d岁 %d财务舒适：月支出%.2f，月收入%.2f，大额存单%d，风险投资%d，年利润%.2f"
              % (age + i, year + i, Monthly_expenditure, Monthly_salary, Financial_management[i], Venture_investment[i], Annual_profit[i]))
    elif Financial_management[i] + Venture_investment[i] > Financial_comfort * 5 and Financial_stage == 3:
        xage[Financial_stage] = age + i
        yfunds[Financial_stage] = Total_funds[i]
        Financial_stage = 4
        print("%d岁 %d财务解锁：月支出%.2f，月收入%.2f，大额存单%d，风险投资%d，年利润%.2f"
              % (age + i, year + i, Monthly_expenditure, Monthly_salary, Financial_management[i], Venture_investment[i], Annual_profit[i]))
    elif Financial_management[i] + Venture_investment[i] > Financial_comfort * 10 and Financial_stage == 4:
        xage[Financial_stage] = age + i
        yfunds[Financial_stage] = Total_funds[i]
        Financial_stage = 5
        print("%d岁 %d财务自由：月支出%.2f，月收入%.2f，大额存单%d，风险投资%d，年利润%.2f"
              % (age + i, year + i, Monthly_expenditure, Monthly_salary, Financial_management[i], Venture_investment[i], Annual_profit[i]))

x = range(age, 66)
plt.rcParams['font.sans-serif'] = ['FangSong']  # 用来正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
plt.figure(figsize=(16, 9))  # 创建绘图对象
# 在当前绘图对象绘图（X轴，Y轴，蓝色虚线，线宽度, 图例）
plt.plot(x, Monetary_Fund, "g-", linewidth=3, label="货币基金")
plt.plot(x, Financial_management, "y-", linewidth=2, label="大额存单")
plt.plot(x, Venture_investment, "r--", linewidth=2, label="风险投资")
plt.plot(x, Total_funds, "k-", linewidth=1, label="总资产")
plt.plot(x, Annual_profit, "b--", linewidth=1, label="年利润")
plt.plot([xage[0], xage[0]], [0, yfunds[0]], "w--", linewidth=2)
plt.annotate("%d年财务生存%d万" % (year + xage[0] - age, yfunds[0]), xy=(xage[0], yfunds[0]), xycoords='data', xytext=(-90, 50),
             textcoords='offset points', fontsize=16, arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2"))
plt.plot([xage[1], xage[1]], [0, yfunds[1]], "r--", linewidth=2)
plt.annotate("%d年财务自足%d万" % (year + xage[1] - age, yfunds[1]), xy=(xage[1], yfunds[1]), xycoords='data', xytext=(-90, 50),
             textcoords='offset points', fontsize=16, arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2"))
plt.plot([xage[2], xage[2]], [0, yfunds[2]], "y--", linewidth=2)
plt.annotate("%d年财务舒适%d万" % (year + xage[2] - age, yfunds[2]), xy=(xage[2], yfunds[2]), xycoords='data', xytext=(-90, 50),
             textcoords='offset points', fontsize=16, arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2"))
plt.plot([xage[3], xage[3]], [0, yfunds[3]], "b--", linewidth=2)
plt.annotate("%d年财务解锁%d万" % (year + xage[3] - age, yfunds[3]), xy=(xage[3], yfunds[3]), xycoords='data', xytext=(-90, 50),
             textcoords='offset points', fontsize=16, arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2"))
plt.plot([xage[4], xage[4]], [0, yfunds[4]], "g--", linewidth=2)
plt.annotate("%d年财务自由%d万" % (year + xage[4] - age, yfunds[4]), xy=(xage[4], yfunds[4]), xycoords='data', xytext=(-90, 50),
             textcoords='offset points', fontsize=16, arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2"))
plt.xticks(x)  # x轴坐标
plt.yticks(range(0, int(Total_funds[65 - age]), 100))  # y轴坐标
plt.grid()  # 显示网格
plt.legend(loc='upper left')  # 图例显示在左上角
plt.xlabel("年龄，单位 年")  # X轴标签
plt.ylabel("总资金，单位 万")  # Y轴标签
plt.title("资金走势图")  # 图标题
plt.show()  # 显示图
